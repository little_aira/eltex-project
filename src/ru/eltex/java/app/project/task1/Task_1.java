package ru.eltex.java.app.project.task1;

import java.util.ArrayList;

public class Task_1 {
    private static int clvCount;
    private static String clvType;
    public static void main(String args[]) throws Exception {
        System.out.println("It`s work!");

        if (args.length < 2)
            throw new Exception("Not enough args");
        if (args.length == 2){
            clvCount = Integer.parseInt(args[0]);
            clvType = args[1];
        }
        ArrayList<ICrudAction> arr = new ArrayList<ICrudAction>();
        for (int i = 0; i < clvCount ; i++) {
            switch (clvType){
                case "Tshirt":
                    Tshirt tsh = new Tshirt();
                    tsh.read();
                    arr.add(tsh);
                    break;
                case "Cap":
                    PeakedCap cap = new PeakedCap();
                    cap.read();
                    arr.add(cap);
                    break;
                default:

            }
            System.out.println("Count:" + Clothes.getCount());

        }
        System.out.println("Delete element:");
        arr.get(0).delete();
        System.out.println("Count:" + Clothes.getCount());
    }
}
