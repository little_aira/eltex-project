package ru.eltex.java.app.project.task1;

import java.util.Random;
import java.util.Scanner;
import java.util.UUID;

public abstract class Clothes implements ICrudAction{

    private String productId; //ID товара
    private String productName; //название товара
    private int productCost; //цена
    private String manufactureFirm; //фирма-производитель товара

    private static int count = 0; //счетчик

    public Clothes() {
        productId = UUID.randomUUID().toString();
        create();
    }

    public void create(){
        Random random = new Random();
        productName = random.nextInt(100) + "Product";
        productCost = random.nextInt(100);
        manufactureFirm = "Firm" + random.nextInt(100);
        count++;
    }
    public void read(){
        System.out.println("ID: "+ this.productId +  "\nName: " + this.productName + "\nPrice: " + this.productCost + "\nFirm: " + this.manufactureFirm);
    }
    public void delete(){
        productId = null;
        productName = null;
        productCost = 0;
        manufactureFirm = null;
        count--;
        System.out.println("Delete complete");

    }
    public void update(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter name of product: ");
        this.productName = scanner.nextLine();
        System.out.println("Enter price: ");
        this.productCost = scanner.nextInt();
        System.out.println("Enter manufacture of product: ");
        this.manufactureFirm = scanner.next();
    }
    public static int getCount() {
        return count;
    }

    public static void setCount(int count) {
        Clothes.count = count;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public double getProductCost() {
        return productCost;
    }

    public void setProductCost(int productCost) {
        this.productCost = productCost;
    }

    public String getManufactureFirm() {
        return manufactureFirm;
    }

    public void setManufactureFirm(String manufactureFirm) {
        this.manufactureFirm = manufactureFirm;
    }
}
