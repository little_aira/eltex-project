package ru.eltex.java.app.project.task1;

public class PeakedCap extends Clothes {
    private String typeCap;
    private String colorCap;

    public void create(){
        super.create();
        typeCap = "Cap_type";
        colorCap = "Indigo";
    }

    public void read(){
        super.read();
        System.out.println("Type: " + typeCap + " Color: " + colorCap);
    }
}
