package ru.eltex.java.app.project.task1;

public class Tshirt extends Clothes {
    private String typeTshirt;


    public Tshirt() {
        typeTshirt = "Polo";
    }

    public void create(){
        super.create();
        typeTshirt = "Shirt_type";
    }

    public void read(){
        super.read();
        System.out.println("Type: " + typeTshirt);
    }
}
