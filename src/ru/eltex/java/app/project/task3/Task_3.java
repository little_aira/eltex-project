package ru.eltex.java.app.project.task3;

import ru.eltex.java.app.project.task1.Clothes;
import ru.eltex.java.app.project.task1.PeakedCap;
import ru.eltex.java.app.project.task1.Tshirt;
import ru.eltex.java.app.project.task2.Credentials;
import ru.eltex.java.app.project.task2.Order;
import ru.eltex.java.app.project.task2.Orders;
import ru.eltex.java.app.project.task2.ShoppingCart;

import java.util.ArrayList;

public class Task_3 {
    public static void main(String[] args) {

        Credentials clientOne = new Credentials("Vanessa", "Paradi", "Kirillovna", "vanpas@mail.ru");
        Credentials clientTwo = new Credentials("Oleg", "Ulyakov", "Evgenovich", "ulya@gmail.com");

        ShoppingCart<Clothes> cart = new ShoppingCart<>();
        Orders<Order> ords = new Orders<>();

        cart.add(new Tshirt());
        cart.add(new PeakedCap());

        Order ord = new Order(cart, clientOne);

        ords.addOrder(ord);

        cart.showAll();
        ords.showAll();
    }
}
