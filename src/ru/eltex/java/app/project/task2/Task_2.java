package ru.eltex.java.app.project.task2;

import ru.eltex.java.app.project.task1.Clothes;
import ru.eltex.java.app.project.task1.PeakedCap;
import ru.eltex.java.app.project.task1.Tshirt;

import java.util.ArrayList;

public class Task_2 {
    private static int clvCount;
    private static String clvType;

    public static void main(String[] args) throws Exception {
        System.out.println("It`s work!");

        if (args.length < 2)
            throw new Exception("Not enough args");
        if (args.length == 2) {
            clvCount = Integer.parseInt(args[0]);
            clvType = args[1];
        }
        Credentials clientOne = new Credentials("Vanessa", "Paradi", "Kirillovna", "vanpas@mail.ru");
        Credentials clientTwo = new Credentials("Oleg", "Ulyakov", "Evgenovich", "ulya@gmail.com");


        ArrayList<Clothes> arr = new ArrayList<>();
        for (int i = 0; i < clvCount; i++) {
            switch (clvType) {
                case "Tshirt":
                    Tshirt tsh = new Tshirt();
                    tsh.read();
                    arr.add(tsh);
                    break;
                case "Cap":
                    PeakedCap cap = new PeakedCap();
                    cap.read();
                    arr.add(cap);
                    break;
                default:

            }

        }

        ShoppingCart cartOne = new ShoppingCart();
        ShoppingCart cartTwo = new ShoppingCart();

        cartOne.add(arr.get(0));

        cartTwo.add(arr.get(1));
        cartTwo.add(arr.get(2));

        Orders orders = new Orders();
        orders.checkout(cartOne, clientOne);
        orders.checkout(cartTwo, clientTwo);


        System.out.println("Orders:");
        orders.showAll();


        System.out.println("\n First cart:");
        cartOne.showAll();
        System.out.println("\n Second cart:");
        cartTwo.showAll();

    }
}
