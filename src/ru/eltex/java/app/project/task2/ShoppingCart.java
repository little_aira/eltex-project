package ru.eltex.java.app.project.task2;

import ru.eltex.java.app.project.task1.Clothes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.TreeSet;

public class ShoppingCart <T extends Clothes> {

    private ArrayList<T> cart;
    private TreeSet<String> trset;

    public ShoppingCart(){
        cart = new ArrayList<>();
        trset = new TreeSet<>();
    }

    public int getCount() {
        return cart.size();
    }

    public void add (T elem){
        cart.add(elem);
        trset.add(elem.getProductId());
    }

    public void delete (T elem){
        cart.remove(elem);
        trset.remove(elem.getProductId());
    }

    public void showAll (){
        for (T elem: cart){
            elem.read();
        }
    }
}
