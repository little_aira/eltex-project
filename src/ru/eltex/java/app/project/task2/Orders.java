package ru.eltex.java.app.project.task2;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class Orders <T extends Order>  {

    private LinkedList<T> ords;
    private Map<Long, Order> timeMap;


    public Orders() {
        ords = new LinkedList<>();
        timeMap = new HashMap<>();
    }

    public void checkout(ShoppingCart cart, Credentials cred){
        T ord = (T) new Order(cart, cred);
        timeMap.put(ord.getCreationTime().getTime(), ord);
        ords.add(ord);
    }

    public void checkOrder(){
        for (T elem: ords) {
            if(elem.getStateOrder().equals(StateOrder.PROCESSED)){
                ords.remove(elem);
            }
            if(!elem.timeout()){
                ords.remove(elem);
            }
        }
    }

    public void showAll(){
        for (T elem: ords){
            elem.read();
        }
    }
    public void addOrder(T ord) {
        ords.add(ord);
    }

}
