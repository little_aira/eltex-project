package ru.eltex.java.app.project.task2;

public enum StateOrder {
    AWAITING, PROCESSED;
}
