package ru.eltex.java.app.project.task2;

import java.util.Date;
import java.util.UUID;

public class Order {

    private StateOrder stateOrder;
    private Date waitingTime;
    private Date creationTime;
    private String orderId;

    private ShoppingCart cart;
    private Credentials client;

    public Order(ShoppingCart cart, Credentials client) {
        this.cart = cart;
        this.client = client;
        stateOrder = StateOrder.AWAITING;
        creationTime = new Date();
        orderId = UUID.randomUUID().toString();
    }
    public boolean timeout() {
        Date curr = new Date();
        return curr.getTime() < creationTime.getTime() + waitingTime.getTime();
    }

    public void read() {
        System.out.println("Order: " + orderId + " status " + stateOrder.toString() + " " + creationTime);
        System.out.println(client.getClientName()+" "+client.getClientSurname()+" количество товаров:" + cart.getCount());
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public StateOrder getStateOrder (){
        return stateOrder;
    }
}
