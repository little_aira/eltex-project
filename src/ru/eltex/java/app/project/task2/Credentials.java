package ru.eltex.java.app.project.task2;

import java.util.UUID;

public class Credentials {

    private String clientId;
    private String clientName;
    private String clientSurname;
    private String clientPatronymic;
    private String clientEmail;

    public Credentials(String clientName, String clientSurname, String clientPatronymic, String clientEmail) {
        clientId  = UUID.randomUUID().toString();
        this.clientName = clientName;
        this.clientSurname = clientSurname;
        this.clientPatronymic = clientPatronymic;
        this.clientEmail = clientEmail;
    }


    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getClientSurname() {
        return clientSurname;
    }

    public void setClientSurname(String clientSurname) {
        this.clientSurname = clientSurname;
    }

    public String getClientPatronymic() {
        return clientPatronymic;
    }

    public void setClientPatronymic(String clientPatronymic) {
        this.clientPatronymic = clientPatronymic;
    }

    public String getClientEmail() {
        return clientEmail;
    }

    public void setClientEmail(String clientEmail) {
        this.clientEmail = clientEmail;
    }
}
